package cluster;

import config.SensorNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 17/08/2016.
 */
public class Kmean {
    private List<SensorNode> points;
    private List<Cluster> clusters;
    private double averValue;

    public int getNUM_CLUSTERS() {
        return NUM_CLUSTERS;
    }

    public void setNUM_CLUSTERS(int NUM_CLUSTERS) {
        this.NUM_CLUSTERS = NUM_CLUSTERS;
    }

    private int NUM_CLUSTERS;
    final static int MAX_COORDINATE = 100;
    final static int MIN_COORDINATE = 0;

    public double getAverValue() {
        return averValue;
    }

    private void setAverValue() {
        int value=0;
        for (Cluster cluster: this.clusters) {
            int count=0, sum=0;
            for(SensorNode point: cluster.getPoints()){
                sum+= SensorNode.getDistance(point, cluster.getCentrePoint());
                count++;
            }
            if(count!=0){
                value+=sum/count;
            }
        }
        this.averValue= value;
    }

    public List<Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(List<Cluster> clusters) {
        this.clusters = clusters;
    }

    public List<SensorNode> getPoints() {
        return points;
    }

    public void setPoints(List<SensorNode> points) {
        this.points = points;
    }

    public Kmean(List<SensorNode> points, int NUM_CLUSTERS){
        this.points = points;
        this.NUM_CLUSTERS= NUM_CLUSTERS;
        this.clusters = new ArrayList<Cluster>();
        init();
        clustering();
        setAverValue();
    }

    public Kmean(List<List<SensorNode>> points, int NUM_CLUSTERS, int a){
        this.points= new ArrayList<>();
        for(int i=0 ; i< points.size(); i++){
            this.points.addAll(points.get(i));
        }
        this.NUM_CLUSTERS= NUM_CLUSTERS;
        this.clusters = new ArrayList<Cluster>();
        init();
        clustering();
        setAverValue();
    }

    private void init(){
        for(int i = 0; i < NUM_CLUSTERS; i++){
            Cluster cluster = new Cluster(i);
            cluster.setCentrePoint(points.get(i));
            clusters.add(cluster);
        }
    }

    private void clear(){
        for(Cluster cluster: clusters){
            cluster.clearPoints();
        }
    }

    private List<SensorNode> getCentrePoints(){
        List<SensorNode> listCentrePoints = new ArrayList<SensorNode>();
        for(int i = 0; i < NUM_CLUSTERS; i++){
            SensorNode point = clusters.get(i).getCentrePoint();
            listCentrePoints.add(point);
        }
        return listCentrePoints;
    }

    private void updateCentrePoint(){
        for(int i = 0; i < NUM_CLUSTERS; i++ ){
            double x = 0.0;
            double y = 0.0;
            for(SensorNode point: clusters.get(i).getPoints()){
                x += point.x;
                y += point.y;
            }

            int size = clusters.get(i).getPoints().size();
            if(size > 0){
                clusters.get(i).setCentrePoint(new SensorNode(x/size, y/size));
            }
        }
    }

    private void updateCluster(){
        for(int i = 0; i < points.size(); i++){
            Double min = Double.MAX_VALUE;
            int numCluster = 0;
            for(int j = 0; j < NUM_CLUSTERS; j++){
                Cluster cluster = clusters.get(j);
                double distance = SensorNode.getDistance(points.get(i), cluster.getCentrePoint());
                if(distance < min){
                    min = distance;
                    numCluster = j;
                }
            }
            points.get(i).setClusterNumber(numCluster);
            clusters.get(numCluster).addPoint(points.get(i));
        }
        updateCentrePoint();
    }

    private void clustering(){
        boolean finish = false;
        List<SensorNode> oldCentrePointList = null;
        List<SensorNode> newCentrePointList = null;
        while(!finish){
            Double sum = 0.0;
            clear();

            oldCentrePointList = getCentrePoints();

            updateCluster();

            newCentrePointList = getCentrePoints();

            for(int i = 0; i < NUM_CLUSTERS; i++){
                sum += SensorNode.getDistance(newCentrePointList.get(i), oldCentrePointList.get(i));
            }

            if(sum <= 0.0001){
                finish = true;
            }
        }
//        printClusters();
    }

    private void printClusters(){
        for(Cluster cluster: clusters){
            cluster.printCluster();
        }
    }

    public static void main(String[] args){
//        Kmean kmean = new Kmean();
//        kmean.init();
//        kmean.clustering();
    }
}
