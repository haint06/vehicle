package cluster;


import config.SensorNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 17/08/2016.
 */
public class Cluster {
    private List<SensorNode> points;
    private SensorNode centrePoint;
    private int clusterNumber;

    public Cluster() {

    }

    public Cluster(int clusterNumber) {
        super();
        this.clusterNumber = clusterNumber;
        this.points = new ArrayList<SensorNode>();
        this.centrePoint = null;
    }

    public double getAllDis() {
        double d = 0;
        for (SensorNode p : points) {

        }
        return d;
    }

    public List<SensorNode> getPoints() {
        return points;
    }

    public void setPoints(List<SensorNode> points) {
        this.points = points;
    }

    public SensorNode getCentrePoint() {
        return centrePoint;
    }

    public void setCentrePoint(SensorNode centrePoint) {
        this.centrePoint = centrePoint;
    }

    public int getClusterNumber() {
        return clusterNumber;
    }

    public void setClusterNumber(int clusterNumber) {
        this.clusterNumber = clusterNumber;
    }

    public void addPoint(SensorNode p) {
        points.add(p);
    }

    public void clearPoints() {
        points.clear();
    }

    public void printCluster() {
        System.out.println(clusterNumber);
        System.out.println("centrePoint");
        System.out.println(centrePoint.x + ", " + centrePoint.y);
        System.out.println();
        for (SensorNode point : points) {
            System.out.println(point.x + ", " + point.y);
        }
        System.out.println();
    }

    public static void main(String args[]) {
        System.exit(1);
    }
}
