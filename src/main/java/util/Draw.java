package util;

import config.Edge;
import config.Position;

import java.awt.*;
import javax.swing.JFrame;
import java.awt.geom.Ellipse2D;
import java.util.List;
import java.util.Random;

public class Draw extends Canvas{

    public List<Position> positionList;
    public List<Position> positionList1;

    public List<Edge> edges;

    public void paint(Graphics g) {
        Graphics2D g2d= (Graphics2D) g;
        setBackground(Color.WHITE);
//        for(int i=0; i<positionList.size(); i++){
//            Position position= positionList.get(i);
//            if((i+1)== positionList.size()) break;
//            Position position1= positionList.get(i+1);
//            g.drawLine(position.x*5+50, position.y*5+50,position1.x*5+50, position1.y*5+50);
////            g.drawRect(position.x*5+50, position.y*5+50, 1, 1);
//        }
//        g.setColor(Color.GREEN);
//        for(int i=0; i<positionList1.size(); i++){
//            Position position= positionList1.get(i);
//            if((i+1)== positionList1.size()) break;
//            Position position1= positionList1.get(i+1);
//            g.drawLine(position.x*5+50, position.y*5+50,position1.x*5+50, position1.y*5+50);
////            g.drawRect(position.x*5+50, position.y*5+50, 1, 1);
//        }
//        g.setColor(Color.black);
//        g.drawRect(50, 50, 500, 500);
        for(int i=0; i< edges.size(); i++){
            Edge edge= edges.get(i);
            g2d.drawLine((int)edge.source.x, (int)edge.source.y, (int)edge.destination.x, (int)edge.destination.y);
        }
//        setForeground(Color.RED);
    }

    public static void main(String[] args) {
        Draw draw=new Draw();
        JFrame f=new JFrame();
        f.add(draw);
        f.setSize(400,400);
        f.setVisible(true);
    }

}