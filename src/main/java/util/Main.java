package util;

import config.*;

import java.util.Comparator;
import java.util.List;

/**
 * Created by prnc on 16/02/2017.
 */
public class Main {

    public static void main(String args[]){
        MapRoad mapRoad = new MapRoad();
        mapRoad.initSensors(Integer.parseInt(args[0]));
        double alpha = 0.3;
        mapRoad.beta = 0.0;
        mapRoad.individual = new Individual();
        mapRoad.Periods = 100000;
        mapRoad.number_of_robots = Integer.parseInt(args[1]);
        Car [] cars= new Car[5];
        mapRoad.sensorNodes.sort(Comparator.comparingInt(SensorNode::getTimeOverFlow));
        List<SensorNode> sensorNodes= mapRoad.sensorNodes;
        for(int i=0; i< mapRoad.number_of_robots; i++){
            cars[i].schedule.add(sensorNodes.get(i).id);
            int time_value= SensorNode.cost(sensorNodes.get(i), cars[i]);
            sensorNodes.get(i).lastVisited.add(time_value);
            sensorNodes.get(i).overFlow.add(time_value+500);
        }

    }

    public void add(Car car, List<SensorNode> sensorNodes){

        sensorNodes.sort(Comparator.comparingInt(SensorNode::getTimeOverFlow));
        SensorNode sensorNode= sensorNodes.get(0);

    }

}
