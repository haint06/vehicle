package util;

import cluster.Cluster;
import cluster.Kmean;
import config.*;

import java.io.*;
import java.util.*;

/**
 * Created by prnc on 14/02/2017.
 */
public class MapRoad {

    public int current_time_temp;
    public static double beta;
    Kmean kmean;
    public static int Periods;
    public int current_time;
    public int W;
    public int H;
    public static int number_of_sensors;
    public static int number_of_bus;
    public static int number_of_robots;
    public Individual individual;

    public List<Integer> lastVisited;
    public List<Integer> overFlowTimes;

    public List<SensorNode> sensorNodes;
    public List<Car> bus;
    public double sum = 0.0;

    public static MyGraph myGraph;

    public MapRoad() {

    }

    //get bus information from file
    public List<Car> getBusDataFromFile(String fileName) {
        List<Car> cars = new ArrayList<>();
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileName))));
            String line = "";
            List<Position> positions1 = new ArrayList<>();
            List<Position> positions2 = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                if (line.equals("1")) {
                    continue;
                }
                String[] arr = line.split("\t");
                positions1.add(new Position(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]), Integer.valueOf(arr[2])));
                while (!(line = reader.readLine()).equals("2")) {
                    arr = line.split("\t");
                    positions1.add(new Position(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]), Integer.valueOf(arr[2])));
                }
                car1.positions = positions1;
                while ((line = reader.readLine()) != null) {
                    arr = line.split("\t");
                    positions2.add(new Position(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]), Integer.valueOf(arr[2])));
                }
                car2.positions = positions2;
            }
        } catch (IOException e) {

        }
        car1.getAllPositions();
        car2.getAllPositions();
        cars.add(car1);
        cars.add(car2);
        this.bus = cars;

        for (Position position : car1.positions) {
            for (SensorNode sensorNode : sensorNodes) {
                if (position.distanceTo(sensorNode) <= 4) {
                    sensorNode.busVisit.add(position.time);
                }
            }
        }
        for (Position position : car2.positions) {
            for (SensorNode sensorNode : sensorNodes) {
                if (position.distanceTo(sensorNode) <= 4) {
                    sensorNode.busVisit.add(position.time);
                }
            }
        }
//        for(SensorNode sensorNode: sensorNodes){
//            if(!sensorNode.busVisit.isEmpty())
//                System.out.println(sensorNode);
//        }
        return cars;
    }

    //get sensors information from file
    public void initSensors(int index) {
        sensorNodes = new ArrayList<SensorNode>();
        try {
            FileInputStream inputStream = new FileInputStream(new File("test" + index + ".txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            int i = 0;
            while ((line = br.readLine()) != null) {
                String arr[] = line.split("\t");
                sensorNodes.add(new SensorNode(Integer.parseInt(arr[2]), Double.parseDouble(arr[0]), Double.parseDouble(arr[1]), i));
                i++;
            }
        } catch (IOException e) {

        }
        for (SensorNode sensorNode : sensorNodes) {
            sensorNode.overFlow.add(sensorNode.baseTime);
            sensorNode.flowTime = sensorNode.baseTime;
        }
        myGraph = loadRoadData("graph.txt");
        List<Position> positionList = new ArrayList<>();
        for (Edge edge : myGraph.edges) {
            List<Position> list = edge.getPointInLine();
            positionList.addAll(list);
        }
        for (SensorNode sensorNode : sensorNodes) {
            for (Position position : positionList) {
                if (position.distanceTo(sensorNode) <= 4) {
                    sensorNode.contains.add(position);
                }
            }
        }
        System.out.println("done set sensors");
    }


    public void initIndividual(int caseV, double alpha) {
        List<List<Obj>> sensorPaths = new ArrayList<>();
        switch (caseV) {
            case 1: //heuristic
                kmean = new Kmean(sensorNodes, number_of_robots);
                for (Cluster cluster : kmean.getClusters()) {
                    sensorPaths.add(getPathMinimumCost(cluster.getClusterNumber(),
                            cluster.getPoints(), 0, alpha));
                }
                break;
            case 2://no road
                kmean = new Kmean(sensorNodes, number_of_robots);
                for (Cluster cluster : kmean.getClusters()) {
//                    sensorPaths.add(getPath(cluster.getClusterNumber(), cluster.getPoints(), 0));
                    sensorPaths.add(getPathMinimumCostNoRoad(cluster.getClusterNumber(),
                            cluster.getPoints(), 0, alpha));
                }
                break;
            default:
                break;
        }
        individual.path_planning = sensorPaths;
        individual.sensorNodes = sensorNodes;

    }

    public Map<Car, Integer> getCarWithCluster(List<Car> cars, List<Cluster> clusters) {
        Map<Car, Integer> map = new HashMap<>();
        for (int i = 0; i < cars.size(); i++) {
            double val = Integer.MAX_VALUE;
            int id = -1;
            Car car = cars.get(i);
            for (Cluster cluster : clusters) {
                double d = car.distanceToCluster(cluster);
                if (d < val) {
                    val = d;
                    id = cluster.getClusterNumber();
                }
            }
            map.put(car, id);
        }
        return map;
    }

    public int getFitnessSA(List<List<Obj>> objs) {
        int val = 0;
        for (int i = 0; i < objs.size(); i++) {
            List<Obj> objs1 = objs.get(i);
            for (int j = 0; j < objs1.size(); j++) {
                Obj obj = objs1.get(j);
                if (obj.sensorNode.getTimeLastVisited() > obj.sensorNode.getTimeOverFlow1()) {
                    val += obj.sensorNode.getTimeLastVisited() - obj.sensorNode.getTimeOverFlow1();
                }
            }
        }
        return val;
    }

    public int getFitnessSA1(List<List<Obj>> objs) {
        int val = 0;
        for (int i = 0; i < objs.size(); i++) {
            List<Obj> objs1 = objs.get(i);
            for (int j = 0; j < objs1.size(); j++) {
                Obj obj = objs1.get(j);
                if (obj.time > obj.flowTime) {
                    val += obj.time - obj.flowTime;
                }
            }
        }
        return val;
    }

    public List<List<Obj>> cloneList(List<List<Obj>> lists) {
        List<List<Obj>> lists1 = new ArrayList<>();
        for (int i = 0; i < lists.size(); i++) {
            List<Obj> objs = lists.get(i);
            List<Obj> objs1 = new ArrayList<>();
            for (int j = 0; j < objs.size(); j++) {
                Obj obj = new Obj(objs.get(j));
                objs1.add(obj);
            }
            lists1.add(objs1);
        }
        return lists1;
    }

    public List<Obj> cloneList1(List<Obj> lists) {
        List<Obj> lists1 = new ArrayList<>();
        for (int i = 0; i < lists.size(); i++) {
            Obj obj = new Obj(lists.get(i));
            lists1.add(obj);
        }
        return lists1;
    }

    public List<Car> cloneCars(List<Car> cars) {
        List<Car> cars1 = new ArrayList<>();
        for (int i = 0; i < cars.size(); i++) {
            cars1.add(new Car(cars.get(i)));
        }
        return cars1;
    }

    private int update(Car car, List<Obj> objs, int time, double alpha) {
        int time_value = time;
        List<Obj> sensorPath = new ArrayList<>();
        SensorNode current_node = new SensorNode(car.x, car.y, time_value);
        List<Obj> temp = cloneList1(objs);
        for (int i = 0; i < objs.size(); i++) {
            for (Obj obj : temp) {
                obj.tempCost = (int) (alpha * (obj.sensorNode.getTimeOverFlow() - time_value)
                        + (1 - alpha) * SensorNode.getCost(current_node, obj.sensorNode));
            }
            temp.sort(Comparator.comparingDouble(a -> a.tempCost));
//            SensorNode tempSensor = new SensorNode(current_node);
            time_value += SensorNode.getCost(current_node, temp.get(0).sensorNode);
            current_node.addLength(SensorNode.getCost(current_node, temp.get(0).sensorNode));
            current_node = temp.get(0).sensorNode;
            current_node.overFlow.add(time_value + 500);
            current_node.lastVisited.add(time_value);
            current_node.visitTime = time_value;
            sensorPath.add(new Obj(current_node, time_value, current_node.getTimeOverFlow(), time_value + 500));
            temp.remove(0);
            if (i == objs.size() - 1) {
                car.x = current_node.x;
                car.y = current_node.y;
            }
        }
        objs = sensorPath;
        return time_value;
    }

    //get path for one robots without roads
    public List<Obj> getPathMinimumCostNoRoad(int carId, List<SensorNode> sensorNodes, int current_time, double alpha) {
        List<Obj> sensorPath = new ArrayList<>();
        int time_value = current_time;
        SensorNode current_node = new SensorNode(50.0, 50.0, 0);
//        Position cNode= new Position(50, 50, 0);
        int index = 0;
        while (true) {
            for (SensorNode sensorNode : sensorNodes) {
                int a = sensorNode.getTImeByBus2(sensorNode.getTimeOverFlow());
                if (a != -1) {
                    sensorNode.overFlow.add(a + 500);
                    sensorNode.lastVisited.add(a);
                }
            }
            for (SensorNode sensorNode : sensorNodes) {
                sensorNode.tempCost = (int) (alpha * (sensorNode.overFlow.get(sensorNode.overFlow.size() - 1) - time_value)
                        + (1 - alpha) * SensorNode.getCost(current_node, sensorNode));
            }
            Collections.sort(sensorNodes, (a, b) -> Integer.compare(a.tempCost, b.tempCost));
            time_value += SensorNode.getCost(current_node, sensorNodes.get(0));

//            System.out.println(time_value + " " + current_node);
            if (time_value > this.Periods) {
                break;
            }
            sensorNodes.get(0).addLength((int) SensorNode.getCost(current_node, sensorNodes.get(0)));
            current_node = sensorNodes.get(0);
            int overFlow = time_value + 500;
            sensorNodes.get(0).overFlow.add(overFlow);
            sensorPath.add(new Obj(sensorNodes.get(0), time_value));
            sensorNodes.get(0).lastVisited.add(time_value);
        }
        sensorPath = sensorPath;
        return sensorPath;
    }

    //get path for one robot with roads
    public List<Obj> getPathMinimumCost(int carId, List<SensorNode> sensorNodes, int current_time, double alpha) {
        List<Obj> sensorPath = new ArrayList<>();
        int time_value = current_time;
        SensorNode current_node = new SensorNode(50.0, 50.0, -1);
        while (true) {
            for (SensorNode sensorNode : sensorNodes) {
                int a = sensorNode.getTImeByBus2(sensorNode.getTimeOverFlow());
                if (a != -1) {
                    sensorNode.overFlow.add(a + 500);
                    sensorNode.lastVisited.add(a);
                }
            }
            for (SensorNode sensorNode : sensorNodes) {
                sensorNode.tempCost = (int) (alpha * (sensorNode.overFlow.get(sensorNode.overFlow.size() - 1) - time_value)
                        + (1 - alpha) * current_node.minLengthToSensor(sensorNode));
            }
            Collections.sort(sensorNodes, (a, b) -> Integer.compare(a.tempCost, b.tempCost));

            Position position = current_node.lengthToSensor(sensorNodes.get(0));
            time_value += current_node.lengthToPosition1(position);

//            sum += current_node.lengthToPosition1(position);
//            System.out.println(time_value + " " + current_node);
            if (time_value > this.Periods) {
                break;
            }
            sensorNodes.get(0).addLength((int) current_node.lengthToPosition1(position));
            current_node = new SensorNode(position.x, position.y);
            int overFlow = time_value + 500;
            for (SensorNode sensorNode : sensorNodes) {
                if (sensorNode.distanceTo(position.x, position.y) <= 4) {
                    sensorNode.overFlow.add(overFlow);
                    sensorPath.add(new Obj(sensorNode, time_value));
                    sensorNode.lastVisited.add(time_value);
                }
            }
        }
        return sensorPath;
    }

    private List<SensorNode> cloneListSensor(List<SensorNode> sensorNodes) {
        List<SensorNode> nodes = new ArrayList<>();
        for (int i = 0; i < sensorNodes.size(); i++) {
            nodes.add(new SensorNode(sensorNodes.get(i)));
        }
        return nodes;
    }

    public List<Obj> getPathUsedBySA(Car car, List<SensorNode> sensorNodes, double alpha) {
        List<SensorNode> sensorNodes2 = cloneListSensor(sensorNodes);
        List<Obj> sensorPath = new ArrayList<>();
        int time_value = current_time;
        SensorNode current_node = new SensorNode(car.x, car.y, current_time);
        for (int i = 0; i < sensorNodes.size(); i++) {
            for (SensorNode sensorNode : sensorNodes2) {
                sensorNode.tempCost = (int) (alpha * (sensorNode.getTimeOverFlow() - time_value) + (1 - alpha) * SensorNode.getCost(current_node, sensorNode));
            }
            Collections.sort(sensorNodes2,
                    (a, b) -> Integer.compare(a.tempCost, b.tempCost)
            );
            SensorNode tempSensor = new SensorNode(current_node);
            time_value += SensorNode.getCost(current_node, sensorNodes2.get(0));

            current_node = sensorNodes2.get(0);
            current_node.baseTime = time_value + 500;
            current_node.addLength(SensorNode.getCost(current_node, sensorNodes2.get(0)));
            current_node.flowTime = current_node.getTimeOverFlow();
            current_node.lastVisited.add(time_value);
            current_node.overFlow.add(time_value + 500);
            current_node.visitTime = time_value;
            sensorPath.add(new Obj(current_node, time_value, current_node.flowTime, time_value + 500));
            sensorNodes2.remove(0);
        }
        car.x = current_node.x;
        car.y = current_node.y;
        current_time_temp = time_value;
        return sensorPath;
    }

    public void updateOverFlowTime() {

    }

    public void sortDeadlineTimes(List<SensorNode> sensorNodes) {
        Collections.sort(sensorNodes, (a, b) ->
                Integer.compare(a.overFlow.get(a.overFlow.size() - 1), b.overFlow.get(b.overFlow.size() - 1))
        );
    }

    public static void createData(int index) {
        Random rd = new Random();
        List<SensorNode> sensorNodes = new ArrayList<>();
        double x = 0, y = 0;
        for (int i = 0; i < index * 100; i++) {
            boolean flag = true;
            while (flag) {
                x = rd.nextDouble() * 100;
                y = rd.nextDouble() * 100;
                if(Math.sqrt(Math.pow(x- 50, 2)+Math.pow(y-50, 2))> 50){
                    continue;
                }
                boolean flag1 = false;
                for (SensorNode sensorNode : sensorNodes) {
                    if (x == sensorNode.x && y == sensorNode.y) {
                        flag1 = true;
                    }
                }
                flag = flag1;
            }
            sensorNodes.add(new SensorNode(x, y));
        }
        SensorNode center = new SensorNode(50, 50);
        int t = 1;
        for (SensorNode sensorNode : sensorNodes) {
            int v = 10 * SensorNode.getCost(sensorNode, center) / 2;
//            int a= (int)(50 * (1 + 0.01* v));
            sensorNode.baseTime = (int) (75 * (1 + 0.01 * v));
            sensorNode.id = t;
            t++;
        }
        File file = new File("test" + index + ".txt");
        try {
            FileWriter fileWriter = new FileWriter(file);
            for (SensorNode sensorNode : sensorNodes) {
                fileWriter.append(sensorNode.x + "\t" + sensorNode.y + "\t" + sensorNode.baseTime);
                fileWriter.append("\n");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {

        }
        System.out.println("ok set sensors");
    }

    public static void main(String args[]) {
        if(Integer.parseInt(args[2])==0)
            createData(Integer.parseInt(args[0]));
        double fit = 0.0;
        double sum = 0.0;
//        for (int i = 0; i < 15; i++) {    //15 times for test
            MapRoad mapRoad = new MapRoad();
            mapRoad.initSensors(Integer.parseInt(args[0]));
            double alpha = 0.5;
            mapRoad.beta = 0.0;
            Chromosome c1 = new Chromosome();
            mapRoad.individual = new Individual();
//            mapRoad.getBusDataFromFile("bus.txt");
            mapRoad.Periods = 100000;
            mapRoad.number_of_robots = Integer.parseInt(args[1]);
            setInitDomain(mapRoad);
//            SA algorithm
            Fitness f = getSA2(mapRoad, alpha, number_of_robots, beta);
            fit += f.overflow;
            sum += f.sumdistance;
//            Heuristic
            mapRoad.initIndividual(1, alpha);
            fit +=mapRoad.individual.fitness();
            sum += mapRoad.individual.total_distance();
//        }
        String s= "ketqua"+args[0]+ args[1]+".txt";
//                System.out.println("m: "+data.get(m)+" - "+"n: "+number_robots.get(n));
        try{
            FileWriter file= new FileWriter(s);
            file.append((fit/15)+"\n");
            file.append((fit/15)+"\n");
            file.flush();
            file.close();
        }catch (IOException e){

        }
//        System.out.println("overflow " + (fit / 15));
//        System.out.println("distance " + (sum / 15));
//            }
//        }
    }

    public static Chromosome getSA(Chromosome c, double a, int index, Kmean kmean) {
        Random rd = new Random();
//        Chromosome c3= new Chromosome(c.sensors, c.cars, c.current_time);
        Chromosome c1 = new Chromosome(c.sensors, c.cars, c.current_time, c.carPaths);
        c1.initHeuristic(a, kmean);
//        Chromosome c3= c1;
//        int f3= c3.getFitnessSA();
        int f1 = c1.getFitnessSA(index, 0.2);
        System.out.println(f1);
        if (f1 != 0) {
            double alpha = 0.95;
            for (double T = 100; T > 0.5; T *= alpha) {
                for (int i = 0; i < 100; i++) {
                    Chromosome c2 = new Chromosome(c1.sensors, c.cars, c.current_time, c.carPaths);
                    c2.getNeighbor(a);
                    int f2 = c2.getFitnessSA(index, 0.2);
//                    System.err.println(f2);
                    double t3 = rd.nextDouble();
                    if (f2 < f1 || t3 < Math.exp(-Math.abs(f2 - f1) / Math.sqrt(T))) {
                        f1 = f2;
                        c1 = c2;
                    }
                }
            }
        }
        System.out.println(f1);
        return c1;
    }

    public static int[][] nb = new int[101][101];

    public static void setInitDomain(MapRoad mapRoad) {
        for (int x = 0; x <= 100; x++) {
            for (int y = 0; y <= 100; y++) {
                int val = 0;
                for (int i = 0; i < mapRoad.sensorNodes.size(); i++) {
                    if (mapRoad.sensorNodes.get(i).distanceTo(x, y) <= 4) {
                        val++;
                    }
                }
                nb[x][y] = val;
            }
        }
    }

    public MyGraph loadRoadData(String path) {
        int m = 0, x = 0, y = 0, index = 0;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                    new File(path))));
            index = Integer.parseInt(reader.readLine());
            x = Integer.parseInt(reader.readLine());
            y = Integer.parseInt(reader.readLine());
        } catch (IOException e) {

        }
        m = 100 / index;
        List<Vertex> vertices = new ArrayList<>();
        List<Edge> edges = new ArrayList<>();

        double a, b;

        for (int i = -1; i <= index; i++) {
            for (int j = -1; j <= index; j++) {
                a = CreateCar.adjust(x + i * m);
                b = CreateCar.adjust(y + j * m);
                vertices.add(new Vertex(a, b));
            }
        }
//        System.err.println(vertices);

        for (int i = 0; i < vertices.size() - 1; i++) {
            for (int j = i + 1; j < vertices.size(); j++) {
                Vertex u = vertices.get(i);
                Vertex v = vertices.get(j);
                if (u.x == v.x || u.y == v.y) {
                    if (u.distance(v) <= m) {
                        edges.add(new Edge(u, v, u.distance(v)));
                        edges.add(new Edge(v, u, u.distance(v)));
                    }
                }
            }
        }
        MyGraph myGraph = new MyGraph(vertices, edges);
        return myGraph;
    }

    public static List<List<SensorNode>> getSA1(List<SensorNode> sensorNodes, double alpha, int number_of_robots, double beta) {
        Random rd = new Random();
        Kmean kmean = new Kmean(sensorNodes, number_of_robots);
        List<List<SensorNode>> temp = new ArrayList<>();
        for (Cluster cluster : kmean.getClusters()) {
            temp.add(cluster.getPoints());
        }

        double f1 = Chromosome.getFitnessSA(temp, alpha, beta);
        double f3 = f1;
        List<List<SensorNode>> temp1 = gan(sensorNodes, temp);
        System.err.println(f1);
        if (f1 != 0) {
            double alpha1 = 0.9;
            for (double T = 100; T > 0.5; T *= alpha1) {
                for (int i = 0; i < 10; i++) {
                    List<List<SensorNode>> temp2 = getNeighbor(temp);
                    double f2 = Chromosome.getFitnessSA(temp2, alpha, beta);
                    double t3 = rd.nextDouble();
                    if (f2 < f1 || t3 < Math.exp(-Math.abs(f2 - f1) / Math.sqrt(T))) {
                        temp = gan(sensorNodes, temp2);
                        f1 = f2;
                        if (f1 < f3) {
                            f3 = f1;
                            temp1 = gan(sensorNodes, temp2);
                        }
                    }
                }
            }
        }
        System.err.println(f3);
        return gan(sensorNodes, temp1);
    }

    public MapRoad(MapRoad mapRoad) {
        this.sensorNodes = new ArrayList<>();
        for (int i = 0; i < mapRoad.sensorNodes.size(); i++) {
            this.sensorNodes.add(new SensorNode(mapRoad.sensorNodes.get(i)));
        }
        this.current_time_temp = mapRoad.current_time_temp;
        this.kmean = mapRoad.kmean;
        this.current_time = mapRoad.current_time;
        W = mapRoad.W;
        H = mapRoad.H;
        this.lastVisited = mapRoad.lastVisited;
        this.overFlowTimes = mapRoad.overFlowTimes;
        this.bus = mapRoad.bus;
        this.individual = new Individual();
    }

//  Get SA solution
    public static Fitness getSA2(MapRoad mapRoad, double alpha, int number_of_robots, double beta) {
        Random rd = new Random();
        Kmean kmean = new Kmean(mapRoad.sensorNodes, number_of_robots);
        List<List<SensorNode>> temp = new ArrayList<>();
        for (Cluster cluster : kmean.getClusters()) {
            temp.add(cluster.getPoints());
        }
        List<List<Obj>> sensorPaths = new ArrayList<>();
        MapRoad maproad1 = new MapRoad(mapRoad);
        for (List<SensorNode> sensorNodes2 : temp) {
            sensorPaths.add(maproad1.getPathMinimumCost(1,
                    gan1(maproad1.sensorNodes, sensorNodes2), 0, alpha));
        }
        maproad1.individual.path_planning = sensorPaths;
        maproad1.individual.sensorNodes = maproad1.sensorNodes;
        double f1 = maproad1.individual.fitness();
//        System.err.println(f1);
        if (f1 != 0) {
            double alpha1 = 0.9;
            for (double T = 100; T > 0.5; T *= alpha1) {
                for (int i = 0; i < 100; i++) {
                    MapRoad maproad2 = new MapRoad(mapRoad);
                    List<List<SensorNode>> temp2 = gan(maproad2.sensorNodes, getNeighbor(temp));

                    for (List<SensorNode> sensorNodes2 : temp2) {
                        sensorPaths.add(maproad2.getPathMinimumCost(1,
                                gan1(maproad2.sensorNodes, sensorNodes2), 0, alpha));
                    }
                    maproad2.individual.path_planning = sensorPaths;
                    maproad2.individual.sensorNodes = maproad2.sensorNodes;
                    double f2 = maproad2.individual.fitness();
//                    System.out.println(f2);
//                    System.err.println(T+" "+f1);
                    double t3 = rd.nextDouble();
                    if (f2 < f1 || t3 < Math.exp(-Math.abs(f2 - f1) / T)) {
                        f1 = f2;
                        maproad1 = maproad2;
                        temp = temp2;
                    }
                }
            }
        }
//        System.out.println(f1);
        Fitness f = new Fitness();
        f.overflow = f1;
        f.sumdistance = maproad1.individual.total_distance();
        return f;
    }

    public static List<List<SensorNode>> gan(List<SensorNode> sensorNodes, List<List<SensorNode>> lists) {
        List<List<SensorNode>> tmp = new ArrayList<>();
        for (List<SensorNode> sensorNodes1 : lists) {
            List<SensorNode> sensorNodes2 = new ArrayList<>();
            for (SensorNode sensorNode : sensorNodes1) {
                for (SensorNode s : sensorNodes) {
                    if (sensorNode.id == s.id) {
                        sensorNodes2.add(s);
                    }
                }
            }
            tmp.add(sensorNodes2);
        }
        return tmp;
    }

    public static List<SensorNode> gan1(List<SensorNode> sensorNodes, List<SensorNode> lists) {
        List<SensorNode> sensorNodes2 = new ArrayList<>();
        for (SensorNode sensorNode : lists) {
            for (SensorNode s : sensorNodes) {
                if (sensorNode.id == s.id) {
                    sensorNodes2.add(s);
                }
            }
        }
        return sensorNodes2;
    }

    public static List<List<SensorNode>> getNeighbor(List<List<SensorNode>> sensors) {
        List<List<SensorNode>> tmp = Chromosome.cloneList(sensors);
        Random rd = new Random();
        int a = rd.nextInt(tmp.size());
        int b = a;
        while (b == a) {
            b = rd.nextInt(tmp.size());
        }
        List<SensorNode> path1 = tmp.get(a);
        List<SensorNode> path2 = tmp.get(b);
        if (path1.size() != 0) {
            if (path2.size() != 0) {
                int p = rd.nextInt(path1.size());
                int q = rd.nextInt(path2.size());
                SensorNode obj1 = path1.get(p);
                SensorNode obj2 = path2.get(q);
                path2.remove(obj2);
                path1.remove(obj1);
                path1.add(obj2);
                path2.add(obj1);
            }
        }
        return tmp;
    }
}
