package config;

/**
 * Created by prnc on 27/02/2017.
 */
public class Position {
    public int x;
    public int y;

    public boolean isset= true;
    public int time;

    public double distanceToSensor(SensorNode sensorNode){
        return Math.sqrt(Math.pow(x- sensorNode.x, 2)+ Math.pow(y- sensorNode.y, 2));
    }

    public Position(){

    }

    public Position(int x, int y){
        this.x= x;
        this.y= y;
    }

    public double distanceTo(SensorNode sensorNode){
        double val= 0.0;
        val= Math.sqrt(Math.pow(x- sensorNode.x, 2)+ Math.pow(y- sensorNode.y, 2));
        return val;
    }

    public Position(int x, int y, int time) {
        this.x = x;
        this.y = y;
        this.time = time;
    }

    public Position(Position position){
        this.x= position.x;
        this.y= position.y;
        this.time= position.time;
    }

    public boolean equals(Position position){
        if(x== position.x && y== position.y ) return true;
        return false;
    }

    public String toString(){
        return this.x+"\t"+this.y+"\t"+this.time;
    }
}
