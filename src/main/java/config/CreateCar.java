package config;

import util.Draw;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by prnc on 27/02/2017.
 */
public class CreateCar {
    static int time = 0;

    public static void main(String args[]) {
//        Random rd = new Random();
//        Position point1 = new Position(0, 0);
//        Position point2 = new Position(0, 100);
//        Position point3 = new Position(100, 0);
//        Position point4 = new Position(100, 100);
//        List<Position> positionList = new ArrayList<>();
//        positionList.add(new Position(100, 0, 0));
//        while (true) {
//            Position current = positionList.get(positionList.size() - 1);
//            Position position = null;
//            position = getNeighbors(current, 2, positionList);
//            positionList.add(position);
//            if (position.equals(point1) || position.equals(point2) || position.equals(point3) || position.equals(point4))
//                break;
//        }
//        System.out.println(positionList);
//        List<Position> positionList1 = new ArrayList<>();
//        positionList1.add(new Position(0, 0, 0));
//        while (true) {
//            Position current = positionList1.get(positionList1.size() - 1);
//            Position position = null;
//            position = getNeighbors(current, 1, positionList1);
//            positionList1.add(position);
//            if (position.equals(point1) || position.equals(point2) || position.equals(point3) || position.equals(point4))
//                break;
//        }
//        System.out.println(positionList1);
//        try{
//            FileWriter file= new FileWriter("bus.txt");
//            file.append("1\n");
//            for(int i=0; i< positionList.size(); i++){
//                file.append(positionList.get(i).x+"\t"+positionList.get(i).y+"\t"+positionList.get(i).time);
//                file.append("\n");
//            }
//            file.append("2\n");
//            for(int i=0; i< positionList1.size(); i++){
//                file.append(positionList1.get(i).toString());
//                file.append("\n");
//            }
//            file.flush();
//            file.close();
//        }catch (IOException e){
//
//        }
//
//        Draw draw=new Draw();
//        draw.positionList= positionList;
//        draw.positionList1= positionList1;
//        JFrame f=new JFrame();
//        f.add(draw);
//        f.setSize(400,400);
//        f.setVisible(true);

        MyGraph myGraph = createRoadData(12);

        List<Edge> edges = createBusRandom(myGraph.vertices, 12);
        List<Position> positionList = new ArrayList<>();
        positionList.add(new Position(0, 0, 0));
        for (int j = 0; j < edges.size(); j++) {
            Edge edge = edges.get(j);
            List<Position> list = getPointInLine(edge);
            for (int i = 0; i < list.size(); i++) {
                positionList.add(list.get(i));
            }
        }
//        System.out.println("done1");
//        edges = createBus2(myGraph.vertices, 12);
//        List<Position> positionList1 = new ArrayList<>();
//        positionList1.add(new Position(100, 0, 0));
//        time = 0;
//        for (int j = 0; j < edges.size(); j++) {
//            Edge edge = edges.get(j);
//            List<Position> list = getPointInLine(edge);
//            for (int i = 0; i < list.size(); i++) {
//                positionList1.add(list.get(i));
//            }
//        }
//        System.out.println("done2");
        try {
            FileWriter file = new FileWriter("bus2.txt");
            file.append("1\n");
            for (int i = 0; i < positionList.size(); i++) {
                file.append(positionList.get(i).x + "\t" + positionList.get(i).y + "\t" + positionList.get(i).time);
                file.append("\n");
            }
            file.flush();
            file.close();
        } catch (IOException e) {

        }

        Draw draw = new Draw();
        draw.edges= edges;
        JFrame f = new JFrame();
        f.add(draw);
        f.setSize(400, 400);
        f.setVisible(true);

    }

    public static List<Position> getPointInLine(Edge edge) {
        List<Position> positionList = new ArrayList<>();
        Vertex s = edge.source;
        Vertex d = edge.destination;
        if (s.x == d.x) {
            if (s.y < d.y) {
                for (int i = (int) s.y + 1; i <= d.y; i++) {
                    positionList.add(new Position((int) s.x, i, time));
                    time++;
                }
            } else {
                for (int i = (int) s.y - 1; i >= d.y; i--) {
                    positionList.add(new Position((int) s.x, i, time));
                    time++;
                }
            }
        } else {
            if (s.x < d.x) {
                for (int i = (int) s.x + 1; i <= d.x; i++) {
                    positionList.add(new Position(i, (int) s.y, time));
                    time++;
                }
            } else {
                for (int i = (int) s.x - 1; i >= d.x; i--) {
                    positionList.add(new Position(i, (int) s.y, time));
                    time++;
                }
            }
        }
        return positionList;
    }

    private static MyGraph createRoadData(int index) {
        int m = 100 / index;
        Random rd = new Random();
        int x = rd.nextInt(m);
        int y = rd.nextInt(m);
        try{
            FileWriter fileWriter= new FileWriter(new File("graph1.txt"));
            fileWriter.append(index+"\n");
            fileWriter.append(x+"\n");
            fileWriter.append(y+"\n");
            fileWriter.flush();
            fileWriter.close();
        }catch (IOException e){

        }
        List<Vertex> vertices = new ArrayList<>();
        List<Edge> edges = new ArrayList<>();

        double a, b;

        for (int i = -1; i <= index; i++) {
            for (int j = -1; j <= index; j++) {
                a = adjust(x + i * m);
                b = adjust(y + j * m);
                vertices.add(new Vertex(a, b));
            }
        }
        System.err.println(vertices);

        for (int i = 0; i < vertices.size() - 1; i++) {
            for (int j = i + 1; j < vertices.size(); j++) {
                Vertex u = vertices.get(i);
                Vertex v = vertices.get(j);
                if (u.x == v.x || u.y == v.y) {
                    if (u.distance(v) <= m) {
                        edges.add(new Edge(u, v, u.distance(v)));
                        edges.add(new Edge(v, u, u.distance(v)));
                    }
                }
            }
        }
        MyGraph myGraph = new MyGraph(vertices, edges);
        return myGraph;
    }

    private static List<Edge> createBus(List<Vertex> vertices, int index) {
        List<Edge> edges = new ArrayList<>();
        Vertex start1 = new Vertex(0, 0);
        Vertex start = new Vertex(0, 0);
        Random rd = new Random();
        while (true) {
            double a = rd.nextDouble();
            if (a <= 0.08) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 1) {
                        start = vertex;
                        break;
                    }
                }
            } else if (a <= 0.16) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 4) {
                        start = vertex;
                        break;
                    }
                }
            } else if (a <= 0.58) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 2) {
                        start = vertex;
                        break;
                    }
                }
            } else {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 3) {
                        start = vertex;
                        break;
                    }
                }
            }
            edges.add(new Edge(start1, start));
//            System.out.println(start);
            start1 = start;
            if ((start.x == 100 & start.y == 100) || (start.x == 0 & start.y == 100)) break;
        }
        return edges;
    }

    private static List<Edge> createBusRandom(List<Vertex> vertices, int index) {
        List<Edge> edges = new ArrayList<>();
        Vertex start1 = new Vertex(0, 0);
        Vertex start = new Vertex(0, 0);
        Random rd = new Random();
        while (true) {
            double a = rd.nextDouble();
            if (a <= 0.25) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 1) {
                        start = vertex;
                        break;
                    }
                }
            } else if (a <= 0.5) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 4) {
                        start = vertex;
                        break;
                    }
                }
            } else if (a <= 0.75) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 2) {
                        start = vertex;
                        break;
                    }
                }
            } else {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 3) {
                        start = vertex;
                        break;
                    }
                }
            }
            edges.add(new Edge(start1, start));
//            System.out.println(start);
            start1 = start;
            if ((start.x == 100 & start.y == 100) || (start.x == 0 & start.y == 100)) break;
        }
        return edges;
    }

    private static List<Edge> createBus2(List<Vertex> vertices, int index) {
        List<Edge> edges = new ArrayList<>();
        Vertex start1 = new Vertex(100, 0);
        Vertex start = new Vertex(100, 0);
        Random rd = new Random();
        while (true) {
            double a = rd.nextDouble();
            if (a <= 0.08) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 1) {
                        start = vertex;
                        break;
                    }
                }
            } else if (a <= 0.16) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 2) {
                        start = vertex;
                        break;
                    }
                }
            } else if (a <= 0.58) {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 4) {
                        start = vertex;
                        break;
                    }
                }
            } else {
                for (Vertex vertex : vertices) {
                    if (start.direction(vertex, index) == 3) {
                        start = vertex;
                        break;
                    }
                }
            }
            edges.add(new Edge(start1, start));
//            System.out.println(start);
            start1 = start;
            if ((start.x == 100 & start.y == 100) || (start.x == 0 & start.y == 100)) break;
        }
        return edges;
    }

    public static double adjust(double a) {
        if (a < 0) return 0;
        if (a > 100) return 100;
        return a;
    }

    private static Position getNeighbors(Position position, int a, List<Position> positions) {
        List<Position> availblePoint = new ArrayList<>();
        int xPrevious = position.x - 1;
        int xAfter = position.x + 1;
        int yAfter = position.y + 1;
        int yPrevious = position.y - 1;
        if (a == 1) {
            if (xPrevious >= 0)
                if (yAfter <= 100)
                    availblePoint.add(new Position(xPrevious, yAfter, position.time + 1));
            if (xAfter <= 100) {
                availblePoint.add(new Position(xAfter, position.y, position.time + 1));
                if (yPrevious >= 0)
                    availblePoint.add(new Position(xAfter, yPrevious, position.time + 1));
            }
            if (yAfter <= 100) {
                availblePoint.add(new Position(position.x, yAfter, position.time + 1));
                if (xAfter <= 100) {
                    availblePoint.add(new Position(xAfter, yAfter, position.time + 1));
                }
            }
        } else if (a == 2) {
            if (xPrevious >= 0)
                if (yPrevious >= 0)
                    availblePoint.add(new Position(xPrevious, yPrevious, position.time + 1));
            if (xAfter <= 100) {
                availblePoint.add(new Position(position.x, yAfter, position.time + 1));
                if (yAfter <= 100)
                    availblePoint.add(new Position(xAfter, yAfter, position.time + 1));
            }
            if (xPrevious >= 0) {
                availblePoint.add(new Position(xPrevious, position.y, position.time + 1));
                if (yAfter <= 100) {
                    availblePoint.add(new Position(xPrevious, yAfter, position.time + 1));
                }
            }
        } else if (a == 3) {
            if (xPrevious >= 0)
                if (yPrevious >= 0)
                    availblePoint.add(new Position(xPrevious, yPrevious, position.time + 1));
            if (xAfter <= 100) {
                availblePoint.add(new Position(xAfter, position.y, position.time + 1));
                if (yAfter <= 100)
                    availblePoint.add(new Position(xAfter, yAfter, position.time + 1));
            }
            if (yPrevious >= 0) {
                availblePoint.add(new Position(position.x, yPrevious, position.time + 1));
                if (yPrevious >= 0) {
                    availblePoint.add(new Position(xPrevious, yPrevious, position.time + 1));
                }
            }
        } else {
            if (xPrevious >= 0)
                if (yAfter <= 100)
                    availblePoint.add(new Position(xPrevious, yAfter, position.time + 1));
            if (yPrevious >= 0) {
                availblePoint.add(new Position(position.x, yPrevious, position.time + 1));
                if (xAfter <= 100)
                    availblePoint.add(new Position(xAfter, yPrevious, position.time + 1));
            }
            if (xPrevious >= 0) {
                availblePoint.add(new Position(xPrevious, position.y, position.time + 1));
                if (yPrevious >= 0) {
                    availblePoint.add(new Position(xPrevious, yPrevious, position.time + 1));
                }
            }
        }
        Random rd = new Random();
        for (int i = 0; i < availblePoint.size(); i++) {
            availblePoint.get(i).isset = true;
        }
        for (int i = 0; i < availblePoint.size(); i++) {
            for (int j = 0; j < positions.size(); j++) {
                if (availblePoint.get(i).equals(positions.get(j))) availblePoint.get(i).isset = false;
            }
        }
        List<Position> positions1 = new ArrayList<>();
        for (int i = 0; i < availblePoint.size(); i++) {
            if (availblePoint.get(i).isset) positions1.add(availblePoint.get(i));
        }
//        System.out.println(position);
//        System.out.println(positions1);
        int x = rd.nextInt(positions1.size());
        Collections.shuffle(positions1);

        return positions1.get(x);
    }
}
