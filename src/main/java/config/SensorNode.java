package config;

import util.MapRoad;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 14/02/2017.
 */
public class SensorNode {
    public int id;
    public double x;
    public double y;
    public double radius;
    public int baseTime;
    public int tempCost;
    public List<Integer> pathLength= new ArrayList<>();

    public int flowTime;
    public int visitTime;
    public boolean flag;

    public List<Integer> busVisit= new ArrayList<>();

    public static final int Buffer = 1000;
    public static final int Ol = 5;
    public static final int Off = 200;

    public List<Integer> lastVisited= new ArrayList<>();
    public List<Integer> overFlow= new ArrayList<>();

    public List<Position> contains= new ArrayList<>();

    public int sumDistance=0;

    public void addLength(int a){
        sumDistance+= a;
    }

    public int getTimeByBus(int time){
        for(int a: busVisit){
            if(a>time && a<= time+500){
                return a;
            }
        }
        return -1;
    }

    public int getTimeOverFlow(){
        return overFlow.get(overFlow.size()-1);
    }

    public int getTimeOverFlow1(){
        return overFlow.get(overFlow.size()-2);
    }

    public int getTimeLastVisited(){
        return lastVisited.get(lastVisited.size()-1);
    }

    public int getTImeByBus2(int time){
        for(int a: busVisit){
            if(a>time-500 && a<= time){
                return a;
            }
        }
        return -1;
    }

    public Position lengthToSensor(SensorNode sensorNode){
        Position position1= new Position();
        double value= Integer.MAX_VALUE;
        for(Position position: sensorNode.contains){
            double tmp= lengthToPosition(position);
            if(value> tmp){
                value= tmp;
                position1= position;
            }
        }
        return position1;
    }

    public double minLengthToSensor(SensorNode sensorNode){
        double value= Integer.MAX_VALUE;
        for(Position position: sensorNode.contains){
            double tmp= lengthToPosition(position);
            if(value> tmp){
                value= tmp;
            }
        }
        return value;
    }

//    public double minLengthToSensor1(SensorNode sensorNode){
//        double value= Integer.MAX_VALUE;
//        for(Position position: sensorNode.contains){
//            double tmp= lengthToPosition1(position);
//            if(value> tmp){
//                value= tmp;
//            }
//        }
//        return value;
//    }

    public double distanceTo(int x, int y){
        return Math.sqrt(Math.pow(this.x- x, 2)+ Math.pow(this.y-y, 2));
    }

    public double lengthToPosition(Position position){
        double val= Math.abs(x- position.x)+ Math.abs(y- position.y);
        return 0.5*(val)+ (1-0.5)* MapRoad.nb[position.x][position.y];
    }

    public double lengthToPosition1(Position position){
        return Math.abs(x- position.x)+ Math.abs(y- position.y);
    }

    public int clusterNumber;

    public SensorNode() {
        this.overFlow= new ArrayList<>();
        this.lastVisited= new ArrayList<>();
        this.pathLength= new ArrayList<>();
    }

    public SensorNode(SensorNode sensorNode){
        this.x= sensorNode.x;
        this.y= sensorNode.y;
        this.baseTime=sensorNode.baseTime;
        this.overFlow= new ArrayList<>(sensorNode.overFlow);
        this.lastVisited= new ArrayList<>(sensorNode.lastVisited);
        this.pathLength= new ArrayList<>(sensorNode.pathLength);
        this.id= sensorNode.id;
        this.contains= sensorNode.contains;
        this.busVisit= sensorNode.busVisit;
    }

    public SensorNode(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public SensorNode(int baseTime, double x, double y, int id){
        this.baseTime= baseTime;
        this.x= x;
        this.y= y;
        this.id= id;
    }

    public SensorNode(double x, double y, int id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public void setClusterNumber(int clusterNumber) {
        this.clusterNumber = clusterNumber;
    }

    public static double getDistance(SensorNode a, SensorNode b) {
        double value = 0.0;
        value = Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
        return value;
    }

    public static int getCost(SensorNode a, SensorNode b) {
        return (int) (Math.round( Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2))));
    }

    public String toString(){
        if(overFlow.size()>=1)
            return "("+x+","+y+","+overFlow.get(overFlow.size()-1)+")";
        return "("+x+","+y+")";
    }

    public static int cost(SensorNode sensorNode, Car car){
        double value = Math.sqrt(Math.pow(sensorNode.x - car.x, 2) + Math.pow(sensorNode.y - car.y, 2));
        int dis= (int)Math.round(value);
        return dis;
    }

//    public static int cost(SensorNode sensorNode, Car car){
//        double value = Math.sqrt(Math.pow(sensorNode.x - car.x, 2) + Math.pow(sensorNode.y - car.y, 2));
//        int dis= (int)Math.round(value);
//        return dis;
//    }
}
