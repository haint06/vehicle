package config;

/**
 * Created by prnc on 09/03/2017.
 */
public class Vertex {

    public double x;
    public double y;

    public int index;

    public Vertex(){

    }

    public Vertex(double x, double y){
        this.x= x;
        this.y= y;
    }

    public double distance(Vertex vertex){
        return Math.sqrt(Math.pow(this.x- vertex.x, 2)+ Math.pow(this.y- vertex.y, 2));
    }

    public String toString(){
        return "("+x+","+y+")";
    }

    public int direction(Vertex v, int index){
        int m= 100/ index;
        if(this.x== v.x){
            if(this.y> v.y){
                if(distance(v)<= m){
                    return 1;
                }
            }else if(this.y< v.y){
                if(distance(v)<= m){
                    return 3;
                }
            }else{
                return -1;
            }
        }else if(this.y==v.y){
            if(this.x> v.x){
                if(distance(v)<= m){
                    return 4;
                }
            }else if(this.x< v.x){
                if(distance(v)<= m){
                    return 2;
                }
            }else{
                return -1;
            }
        }
        return -1;
    }

}
