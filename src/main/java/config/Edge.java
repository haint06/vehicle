package config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 09/03/2017.
 */
public class Edge {

    public double weight;
    public Vertex source, destination;

    public Edge(){

    }

    public List<Position> getPointInLine() {
        List<Position> positionList = new ArrayList<>();
        Vertex s = this.source;
        Vertex d = this.destination;
        if (s.x == d.x) {
            if (s.y < d.y) {
                for (int i = (int) s.y ; i <= d.y; i++) {
                    positionList.add(new Position((int) s.x, i));
                }
            } else {
                for (int i = (int) s.y ; i >= d.y; i--) {
                    positionList.add(new Position((int) s.x, i));
                }
            }
        } else {
            if (s.x < d.x) {
                for (int i = (int) s.x ; i <= d.x; i++) {
                    positionList.add(new Position(i, (int) s.y));
                }
            } else {
                for (int i = (int) s.x ; i >= d.x; i--) {
                    positionList.add(new Position(i, (int) s.y));
                }
            }
        }
        return positionList;
    }

    public Edge(Vertex source, Vertex destination, double weight){
        this.source= source;
        this.destination= destination;
        this.weight= weight;
    }

    public Edge(Vertex source, Vertex destination){
        this.source= source;
        this.destination= destination;
    }
}
