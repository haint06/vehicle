package config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by prnc on 16/02/2017.
 */
public class Individual {

    public double fitnessValue;
    public List<SensorNode> sensorNodes;
    public List<List<Obj>> path_planning;

    public Individual(){

    }

    public Individual(List<SensorNode> sensorNodes){
        path_planning= new ArrayList<>();
        this.sensorNodes= new ArrayList<>(sensorNodes);
    }

    public int total_distance(){
        int sum= 0;
        for(SensorNode sensorNode: sensorNodes){
            sum+= sensorNode.sumDistance;
        }
        return sum;
    }

    public double fitness(){
        double value= 0.0;
        for(int i=0; i<sensorNodes.size(); i++){
            SensorNode sensorNode= sensorNodes.get(i);
            for(int j=0; j< sensorNode.overFlow.size(); j++){
                int overflow= sensorNode.overFlow.get(j);
                if(j>= sensorNode.lastVisited.size()) break;
                int timeLastVisited= sensorNode.lastVisited.get(j);
                if(timeLastVisited> overflow) value+=timeLastVisited- overflow;
            }
        }
        this.fitnessValue= value;
        return value;
    }

    public double getCurrentFitness(){
        double value= 0.0;
        for(int i=0; i<sensorNodes.size(); i++){
            SensorNode sensorNode= sensorNodes.get(i);
            if(sensorNode.getTimeLastVisited()> sensorNode.getTimeOverFlow())
            value+= sensorNode.getTimeLastVisited()- sensorNode.getTimeOverFlow();
        };
        return value;
    }

}
