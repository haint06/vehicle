package config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 23/03/2017.
 */
public class Pixel {
    public int x;
    public int y;
    public List<SensorNode> sensorNodeList;

    public void add(SensorNode sensorNode){
        sensorNodeList= new ArrayList<>();
        sensorNodeList.add(sensorNode);
    }

    public boolean check(SensorNode sensorNode){
        for(SensorNode sensorNode1: sensorNodeList){
            if(sensorNode1.id== sensorNode.id){
                return true;
            }
        }
        return false;
    }
}
