package config;

import cluster.Cluster;
import cluster.Kmean;
import util.MapRoad;

import java.util.*;

/**
 * Created by prnc on 20/03/2017.
 */
public class Chromosome {

    public List<List<SensorNode>> sensors;
    public List<Car> cars;
    public int current_time;
    public List<CarPath> carPaths= new ArrayList<>();

    public static  double getFitness(List<List<SensorNode>> sensors, double beta) {
        double val = 0;
        double sum=0;
        for (int i = 0; i < sensors.size(); i++) {
            List<SensorNode> objs1 = sensors.get(i);
            for (int j = 0; j < objs1.size(); j++) {
                SensorNode obj = objs1.get(j);
                sum+= obj.sumDistance;
                if (obj.overFlow.size() >= 2) {
                    for(int k=0; k< obj.lastVisited.size(); k++){
                        int overflow= obj.overFlow.get(k);
                        int timeLastVisited= obj.lastVisited.get(k);
                        if(timeLastVisited> overflow) val+=timeLastVisited- overflow;
                    }
                }
            }
        }
        return (beta*val+ (1-beta)*sum);
    }

    public int getFitnessSA(int index, double beta) {
        int val = 0;
        int sum=0;
        for (int i = 0; i < sensors.size(); i++) {
            List<SensorNode> objs1 = sensors.get(i);
            for (int j = 0; j < objs1.size(); j++) {
                SensorNode obj = objs1.get(j);
                sum+= obj.sumDistance;
                if (obj.overFlow.size() >= 2) {
                    if (obj.getTimeOverFlow1() >= 100000) continue;
                    if (obj.getTimeLastVisited() > obj.getTimeOverFlow1()) {
                        val += obj.getTimeLastVisited() - obj.getTimeOverFlow1();
                    }
                }
            }
        }
        return (int)(0.9*val+ (1-0.9)*sum);
    }

    public static double getFitnessSA(List<List<SensorNode>> sensors, double alpha, double beta) {
        double val = 0;
        List<List<SensorNode>> temp= cloneList(sensors);
        for (int i = 0; i < temp.size(); i++) {
            List<SensorNode> sensorNodes = temp.get(i);
            updateHeuristic(sensorNodes, alpha);
        }
        val= getFitness(temp, beta);
        return val;
    }

    public static List<List<SensorNode>> cloneList(List<List<SensorNode>> sensors){
        List<List<SensorNode>> lists= new ArrayList<>();
        for(List<SensorNode> sensorNodes: sensors){
            List<SensorNode> temp= new ArrayList<>();
            for(SensorNode sensorNode: sensorNodes){
                temp.add(new SensorNode(sensorNode));
            }
            lists.add(temp);
        }
        return lists;
    }

    public double fitness(int n) {
        double val = 0;
        for (int i = 0; i < carPaths.size(); i++) {
            List<SensorNode> objs1 = carPaths.get(i).objs;
            for (int j = 0; j < objs1.size(); j++) {
                SensorNode sensorNode = objs1.get(j);
                for (int k = 0; k < sensorNode.overFlow.size(); k++) {
                    int overflow = sensorNode.overFlow.get(k);
                    if(overflow> 100000) break;
                    if (k >= sensorNode.lastVisited.size()) break;
                    int timeLastVisited = sensorNode.lastVisited.get(k);
                    if (timeLastVisited > overflow)
                        val += timeLastVisited - overflow;
                }
            }
        }
        System.err.println(val+" : "+n);
        return 100 * val / (100000 * n);
    }

    public double distance() {
        int val = 0;
        for (int i = 0; i < carPaths.size(); i++) {
            List<SensorNode> objs1 = carPaths.get(i).objs;
            for (int j = 0; j < objs1.size(); j++) {
                SensorNode sensorNode = objs1.get(j);
                val+= sensorNode.sumDistance;
            }
        }
        return val ;
    }

    public Chromosome() {
        sensors = new ArrayList<>();
        cars = new ArrayList<>();
    }

    public void getNeighbor(double alpha) {
        Random rd = new Random();
        int a = rd.nextInt(sensors.size());
        int b = a;
        while (b == a) {
            b = rd.nextInt(sensors.size());
        }
        List<SensorNode> path1 = sensors.get(a);
        List<SensorNode> path2 = sensors.get(b);
        if (path1.size() != 0) {
            if (path2.size() != 0) {
                int p = rd.nextInt(path1.size());
                int q = rd.nextInt(path2.size());
                SensorNode obj1 = path1.get(p);
                SensorNode obj2 = path2.get(q);
                path2.remove(obj2);
                path1.remove(obj1);
                path1.add(obj2);
                path2.add(obj1);
            }
        }
        Map<Car, List<SensorNode>> carListMap = new HashMap<>();
        for (Car car : cars) {
            car.foo = 0;
        }
        for (List list : sensors) {
            double min = Integer.MAX_VALUE;
            Car c = new Car();
            for (Car car : cars) {
                if (car.foo == 0) {
                    double temp = car.minDistanceToList(list);
                    if (temp < min) {
                        min = temp;
                        c = car;
                    }
                }
            }
            carListMap.put(c, list);
            c.foo = 1;
        }
        int maxTime = Integer.MIN_VALUE;
        List<List<SensorNode>> lists = new ArrayList<>();
        List<CarPath> carPaths= new ArrayList<>();
        for (Car car : carListMap.keySet()) {
            CarPath carPath = initHeuristicChildFirst1(car, carListMap.get(car), current_time, alpha);
            lists.add(carPath.objs);
            carPaths.add(carPath);
            maxTime = maxTime > carPath.time ? maxTime : carPath.time;
        }
        this.carPaths= carPaths;
        this.sensors = lists;
        this.current_time = maxTime;
    }

    public Chromosome(List<List<SensorNode>> sensors, List<Car> cars, int current_time, List<CarPath> carPaths) {
        this.sensors = new ArrayList<>();
        for (int i = 0; i < sensors.size(); i++) {
            List<SensorNode> objs = sensors.get(i);
            List<SensorNode> objs1 = new ArrayList<>();
            for (int j = 0; j < objs.size(); j++) {
                SensorNode obj = new SensorNode(objs.get(j));
//                obj.x = objs.get(j).x;
//                obj.y = objs.get(j).y;
//                obj.baseTime = objs.get(j).baseTime;
//                obj.overFlow = new ArrayList<>(objs.get(j).overFlow);
//                obj.lastVisited = new ArrayList<>(objs.get(j).lastVisited);
//                obj.pathLength = new ArrayList<>(objs.get(j).pathLength);
//
//                obj.id = objs.get(j).id;
                objs1.add(obj);
            }
            this.sensors.add(objs1);
        }
        this.cars = new ArrayList<>();
        for (int i = 0; i < cars.size(); i++) {
            this.cars.add(new Car(cars.get(i)));
        }
        this.carPaths= new ArrayList<>();
        for(int i=0; i< carPaths.size(); i++){
            this.carPaths.add(new CarPath(carPaths.get(i)));
        }
        this.current_time = current_time;
    }

    public List<SensorNode> cloneListSensors(List<SensorNode> sensorNodes) {
        List<SensorNode> sensorNodes1 = new ArrayList<>();
        for (int i = 0; i < sensors.size(); i++) {
            List<SensorNode> s1 = sensors.get(i);
            for (int j = 0; j < s1.size(); j++) {
                for (int k = 0; k < sensorNodes.size(); k++) {
                    if (sensorNodes.get(k).id == s1.get(j).id) {
                        sensorNodes1.add(s1.get(j));
                        break;
                    }
                }
            }
        }
        return sensorNodes1;
    }

    public void initHeuristic(double alpha, Kmean kmean) {
        if (current_time != 0) kmean = new Kmean(sensors, kmean.getNUM_CLUSTERS(), 1);
        int maxTime = 0;
        Map<Car, Cluster> carListMap = new HashMap<>();
        for (Car car : cars) {
            car.foo = 0;
        }
        for (Cluster cluster : kmean.getClusters()) {
            double min = Integer.MAX_VALUE;
            Car c = new Car();
            for (Car car : cars) {
                if (car.foo == 0) {
                    double d = car.distanceToCluster(cluster);
                    if (min > d) {
                        min = d;
                        c = car;
                    }
                }
            }
            carListMap.put(c, cluster);
            c.foo = 1;
        }
        maxTime = Integer.MIN_VALUE;
        List<List<SensorNode>> lists = new ArrayList<>();
        for (Car car : carListMap.keySet()) {
            CarPath carPath = initHeuristicChildFirst(car, carListMap.get(car).getPoints(), current_time, alpha);
            lists.add(carPath.objs);
            carPaths.add(carPath);
            maxTime = maxTime > carPath.time ? maxTime : carPath.time;
        }
        this.sensors = lists;
        current_time = maxTime;
    }

    public void setIn(double alpha){
        for(int i=0; i<carPaths.size(); i++){
            getPathMinimumCostIntervalTime(carPaths.get(i), alpha);
        }
    }

    public CarPath initHeuristicChildFirst(Car car, List<SensorNode> sensorNodes, int current_time, double alpha) {
        int time_value = current_time;
        SensorNode current_node = new SensorNode(car.x, car.y);
        for (SensorNode obj : sensorNodes) {
            obj.flag = true;
        }
        int b = 0;
        for (int i = 0; i < sensorNodes.size(); i++) {
            for (SensorNode sensorNode : sensorNodes) {
                int a = sensorNode.getTImeByBus2(sensorNode.getTimeOverFlow());
                if (a != -1) {
                    sensorNode.overFlow.add(a + 500);
                    sensorNode.lastVisited.add(a);
                }
            }
            for (SensorNode obj : sensorNodes) {
                if (obj.flag) {
                    b = (int) (alpha * (obj.getTimeOverFlow() - time_value)
                            + (1 - alpha) * current_node.minLengthToSensor(obj));
                    obj.tempCost = b;
                } else {
                    b = Integer.MAX_VALUE;
                    obj.tempCost = b;
                }
            }
            sensorNodes.sort(Comparator.comparingInt(a -> a.tempCost));
            Position position = current_node.lengthToSensor(sensorNodes.get(0));
            int length =(int) current_node.lengthToPosition1(position);
            time_value += length;
            current_node = new SensorNode(position.x, position.y);
            int overFlow = time_value + 500;
            sensorNodes.get(0).addLength(length);
            for(SensorNode sensorNode:sensorNodes){
                if(sensorNode.distanceTo(position.x, position.y)<= 4){
                    sensorNode.overFlow.add(overFlow);
                    sensorNode.lastVisited.add(time_value);
                    sensorNode.flag= false;
                }
            }
            boolean f= false;
            for(SensorNode sensorNode: sensorNodes){
                if(sensorNode.flag){
                    f= true;
                }
            }
            if(!f) break;
//            current_node = sensorNodes.get(0);
//            current_node.addLength(length);
//            current_node.flag = false;
//            current_node.overFlow.add(time_value + 500);
//            current_node.lastVisited.add(time_value);
        }
        car.x = current_node.x;
        car.y = current_node.y;
        return new CarPath(time_value, sensorNodes, car);
    }

    public static void updateHeuristic( List<SensorNode> sensorNodes, double alpha) {
        int time_value = 0;
        SensorNode current_node = new SensorNode(50, 50);
        for (SensorNode obj : sensorNodes) {
            obj.flag = true;
        }
        int b = 0;
        for (int i = 0; i < sensorNodes.size(); i++) {
//            for (SensorNode sensorNode : sensorNodes) {
//                int a = sensorNode.getTImeByBus2(sensorNode.getTimeOverFlow());
//                if (a != -1) {
//                    sensorNode.overFlow.add(a + 500);
//                    sensorNode.lastVisited.add(a);
//                    sensorNode.flag= false;
//                }
//            }
            for (SensorNode sensorNode : sensorNodes) {
                if (sensorNode.flag) {
                    b = (int) (alpha * (sensorNode.getTimeOverFlow() - time_value)
                            + (1 - alpha) * current_node.minLengthToSensor(sensorNode));
                    sensorNode.tempCost = b;
                } else {
                    b = Integer.MAX_VALUE;
                    sensorNode.tempCost = b;
                }
            }
            sensorNodes.sort(Comparator.comparingInt(a -> a.tempCost));
            Position position = current_node.lengthToSensor(sensorNodes.get(0));
            int length =(int) current_node.lengthToPosition1(position);
            time_value += length;
            current_node = new SensorNode(position.x, position.y);
            int overFlow = time_value + 500;
            sensorNodes.get(0).addLength(length);
            for(SensorNode sensorNode : sensorNodes)
                if (sensorNode.distanceTo(position.x, position.y) <= 4) {
                    sensorNode.overFlow.add(overFlow);
                    sensorNode.lastVisited.add(time_value);
                    sensorNode.flag = false;
                }
            boolean f= false;
            for(SensorNode sensorNode: sensorNodes){
                if(sensorNode.flag){
                    f= true;
                }
            }
            if(!f) break;
        }
    }

    public void getPathMinimumCostIntervalTime(CarPath carPath, double alpha) {
        List<SensorNode> sensorPath = new ArrayList<>();
        int time_value = carPath.time;
        SensorNode current_node = new SensorNode(carPath.car.x, carPath.car.y, -1);
        int index = 0;
        while (true) {
            for (SensorNode sensorNode : carPath.objs) {
                int a = sensorNode.getTImeByBus2(sensorNode.getTimeOverFlow());
                if (a != -1) {
                    sensorNode.overFlow.add(a + 500);
                    sensorNode.lastVisited.add(a);
                }
            }
            for (SensorNode sensorNode : carPath.objs) {
                sensorNode.tempCost = (int) (alpha * (sensorNode.overFlow.get(sensorNode.overFlow.size() - 1) - time_value)
                        + (1 - alpha) * current_node.minLengthToSensor(sensorNode));
//                System.out.println(current_node.minLengthToSensor(sensorNode));
            }
            Collections.sort(carPath.objs, (a, b) -> Integer.compare(a.tempCost, b.tempCost));
//            int i=0;
//            while(true){
//                if(carPath.objs.get(0).x== current_node.x& carPath.objs.get(0).y== current_node.y){
//                    i++;
//                }else{
//                    break;
//                }
//            }
            Position position = current_node.lengthToSensor(carPath.objs.get(0));
            time_value += current_node.lengthToPosition1(position);
            carPath.objs.get(0).addLength((int) current_node.lengthToPosition1(position));
//            System.out.println(time_value + " " + current_node);
            if (time_value > MapRoad.Periods) {
                break;
            }
            current_node = new SensorNode(position.x, position.y);
            int overFlow = time_value + 500;
            for(SensorNode sensorNode: carPath.objs){
                if(sensorNode.distanceTo(position.x, position.y)<= 4){
                    sensorNode.overFlow.add(overFlow);
//                    sensorPath.add(new Obj(sensorNode, time_value));
                    sensorNode.lastVisited.add(time_value);
//                    int x=7;
                }
            }
        }
    }

    public CarPath initHeuristicChildFirst1(Car car, List<SensorNode> sensorNodes, int current_time, double alpha) {
        int time_value = current_time;
        SensorNode current_node = new SensorNode(car.x, car.y);
        for (SensorNode obj : sensorNodes) {
            obj.flag = true;
        }
        int b;
        for (int i = 0; i < sensorNodes.size(); i++) {
            for (SensorNode sensorNode : sensorNodes) {
                int a = sensorNode.getTImeByBus2(sensorNode.getTimeOverFlow());
                if (a != -1) {
                    sensorNode.overFlow.add(a + 500);
                    sensorNode.lastVisited.add(a);
                }
            }
            for (SensorNode obj : sensorNodes) {
                if (obj.flag) {
                    b = (int) (alpha * (obj.getTimeOverFlow() - time_value)
                            + (1 - alpha) * current_node.minLengthToSensor(obj));
//                    System.out.println(current_node.minLengthToSensor(obj));
                    obj.tempCost = b;
                } else {
                    b = Integer.MAX_VALUE;
                    obj.tempCost = b;
                }
            }
            sensorNodes.sort(Comparator.comparingInt(a -> a.tempCost));
            Position position = current_node.lengthToSensor(sensorNodes.get(0));
            int length =(int) current_node.lengthToPosition1(position);
            time_value += length;
            current_node = new SensorNode(position.x, position.y);
            int overFlow = time_value + 500;
            sensorNodes.get(0).addLength(length);
            for(SensorNode sensorNode:sensorNodes){
                if(sensorNode.distanceTo(position.x, position.y)<= 4){
                    if (sensorNode.pathLength.size() >= 1) sensorNode.pathLength.remove(sensorNode.pathLength.size() - 1);
                    if (sensorNode.overFlow.size() >= 1) sensorNode.overFlow.remove(sensorNode.overFlow.size() - 1);
                    if (sensorNode.lastVisited.size() >= 1)
                        sensorNode.lastVisited.remove(sensorNode.lastVisited.size() - 1);
                    sensorNode.overFlow.add(overFlow);
                    sensorNode.lastVisited.add(time_value);
                    sensorNode.flag= false;
                }
            }
            boolean f= false;
            for(SensorNode sensorNode: sensorNodes){
                if(sensorNode.flag){
                    f= true;
                }
            }
            if(!f) break;
//            current_node.addLength(length);
//            current_node.flag = false;
//            current_node.overFlow.add(time_value + 500);
//            current_node.lastVisited.add(time_value);
        }
        car.x = current_node.x;
        car.y = current_node.y;
        return new CarPath(time_value, sensorNodes, car);
    }

    public static void main(String args[]) {

    }
}
