package config;

/**
 * Created by prnc on 22/02/2017.
 */
public class Obj {
    public SensorNode sensorNode;
    public int time;
    public int flowTime;
    public int newFlowTime;

    public double tempCost;

    public Obj(SensorNode sensorNode, int time){
        this.sensorNode= sensorNode;
        this.time= time;
    }

    public Obj(SensorNode sensorNode, int time, int flowTime){
        this.sensorNode= sensorNode;
        this.time= time;
        this.flowTime= flowTime;
    }

    public Obj(SensorNode sensorNode, int time, int flowTime, int newFlowTime){
        this.sensorNode= sensorNode;
        this.time= time;
        this.flowTime= flowTime;
        this.newFlowTime= newFlowTime;
    }

    public Obj(Obj obj){
        this.sensorNode= new SensorNode(obj.sensorNode);
        this.time= obj.time;
    }

    public String toString(){
        return "("+sensorNode.getTimeOverFlow()+" "+ sensorNode.getTimeLastVisited()+")";
    }
}
