package config;

import cluster.Cluster;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by prnc on 14/02/2017.
 */
public class Car {

    public int time_value=0;

    public int foo;

    public int id;

    public List<Integer> schedule;

    public List<Position> positions;

    public double x;
    public double y;

    public Car(){

    }

    public Car(int id) {
        this.id = id;
    }

    public Car(Car car){
        this.x= car.x;
        this.y= car.y;
        this.id= car.id;
    }

    public Car(int id, double x, double y){
        this.id= id;
        this.x= x;
        this.y= y;
    }

    public Position getPositionAtTime(int time) {
        for (int i = 0; i < positions.size(); i++) {
            if (i < positions.size() - 1) {
                if (time >= positions.get(i).time && time < positions.get(i + 1).time) {
                    return positions.get(i);
                }
            }
        }
        return positions.get(positions.size() - 1);
    }

    public double distanceToSensorNode(SensorNode sensorNode){
        double val= 0.0;
        val= Math.sqrt(Math.pow(this.x- sensorNode.x, 2)+ Math.pow(this.y- sensorNode.y, 2));
        return val;
    }

    public double minDistanceToList(List<SensorNode> objs){
        double val=Integer.MAX_VALUE;
        for(SensorNode obj: objs){
            double temp= distanceToSensorNode(obj);
            if(temp< val){
                val= temp;
            }
        }
        return val;
    }

    public void getAllPositions() {
        List<Position> positionList1 = new ArrayList<>();
        for (int i = 0; i < positions.size(); i++) {
            positionList1.add(new Position(positions.get(i)));
        }
//        System.out.println(positionList1);
        List<Position> positionList = new ArrayList<>(positions);
        boolean flag = false;
        while (true) {
            Collections.reverse(positionList);
            int extraTime = 10 + positionList.get(0).time;
            for (int i = 0; i < positionList.size(); i++) {
                int a = extraTime + i;
                positionList.get(i).time = a;
                if (a >= 100000) {
                    flag = true;
                    break;
                }
            }
            for (int i = 0; i < positionList.size(); i++) {
                positionList1.add(new Position(positionList.get(i)));
            }
//            System.out.println(positionList1);
            if (flag) break;
        }
//        try{
//            FileWriter fileWriter= new FileWriter("allBus.txt");
//            for(int i=0; i<positionList1.size(); i++){
//                fileWriter.append(positionList1.get(i)+"\n");
//            }
//            fileWriter.flush();
//            fileWriter.close();
//        }catch (IOException e){
//
//        }
        this.positions = positionList1;
    }

    private double distanToSensor(SensorNode sensorNode) {
        return Math.sqrt(Math.pow(this.x - sensorNode.x, 2) + Math.pow(this.y - sensorNode.y, 2));
    }

    public double distanceToCluster(Cluster cluster) {
        double val = Integer.MAX_VALUE;
        for (SensorNode sensorNode : cluster.getPoints()) {
            if (this.distanToSensor(sensorNode) < val) {
                val= this.distanToSensor(sensorNode);
            }
        }
        return val;
    }


}
