package config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 25/02/2017.
 */
public class CarPath {
    public Car car;
    public List<SensorNode> objs= new ArrayList<>();
    public int time;

    public CarPath(int time, List<SensorNode> objs){
        this.time= time;
        this.objs= objs;
    }

    public CarPath(CarPath c){
        this.time= c.time;
        this.car= new Car(c.car);
        this.objs= new ArrayList<>();
        for(int i=0; i< c.objs.size(); i++){
            SensorNode node= new SensorNode(c.objs.get(i));
            this.objs.add(node);
        }
    }

    public CarPath(int time, List<SensorNode> objs, Car car){
        this.time= time;
        this.objs= objs;
        this.car= car;
    }
}
