package config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prnc on 09/03/2017.
 */
public class MyGraph {

    public List<Vertex> vertices;
    public List<Edge> edges;

    public MyGraph(){
        vertices= new ArrayList<>();
        edges= new ArrayList<>();
    }

    public MyGraph(List<Vertex> vertices, List<Edge> edges){
        this.edges= edges;
        this.vertices= vertices;
    }

    public void addVertex(Vertex v){
        vertices.add(v);
    }

    public void addEdge(Vertex u, Vertex v, double w){
        edges.add(new Edge(u, v, w));
        edges.add(new Edge(v, u, w));
    }

}
